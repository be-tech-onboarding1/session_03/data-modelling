const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Product Name is required"]
	},
	description : {
		type : String,
		required : [true, "Description is required"]
	},
	price : { 
		type : Number,
		required : [true, "Price is required"]
	},
	stocks : {
		type : Number,
		required : [true, "Stock quantity is required"]
	}
})

module.exports = mongoose.model("Product", productSchema);