const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Product Name is required"]
	},
	description : {
		type : String,
		required : [true, "Description is required"]
	},
	price : { 
		type : Number,
		required : [true, "Price is required"]
	},
	stocks : {
		type : Number,
		required : [true, "Stock quantity is required"]
	}
	orders : [ 
		{
			userId : {
				type : String,
				required : [true, "User ID is required"]
			},
			orderedOn : {
				type : Date,
				default : new Date()
			}
		}
	]
})

module.exports = mongoose.model("Product", productSchema);